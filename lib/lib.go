package lib

import (
	"crypto/md5"
	"encoding/hex"
	"strconv"
)

//create md5 string
func Strtomd5(s string) string {
	h := md5.New()
	h.Write([]byte(s))
	rs := hex.EncodeToString(h.Sum(nil))
	return rs
}

//password hash function
func Pwdhash(str string) string {
	return Strtomd5(str)
}

func StringsToJson(str string) string {
	rs := []rune(str)
	jsons := ""
	for _, r := range rs {
		rint := int(r)
		if rint < 128 {
			jsons += string(r)
		} else {
			jsons += "\\u" + strconv.FormatInt(int64(rint), 16) // json
		}
	}

	return jsons
}

//判断数组中是否存在某个元素
func IsExistInArry(arry []int64, obj int64) bool {
	var est bool
	for _, bb := range arry {
		if obj == bb {
			est = true
			break
		}
	}
	return est
}
