package admin

import (
	"github.com/astaxie/beego"
	"github.com/beego/admin/src/rbac"
)

func router() {
	beego.Router("/", &rbac.MainController{}, "*:Index")
	beego.Router("/public/index", &rbac.MainController{}, "*:Index")
	beego.Router("/public/login", &rbac.MainController{}, "*:Login")
	beego.Router("/public/logout", &rbac.MainController{}, "*:Logout")
	beego.Router("/public/changepwd", &rbac.MainController{}, "*:Changepwd")
	beego.Router("/public/myinfo", &rbac.MainController{}, "*:MyInfo") //add by kang@20181120

	beego.Router("/rbac/user/AddUser", &rbac.UserController{}, "*:AddUser")
	beego.Router("/rbac/user/UpdateUser", &rbac.UserController{}, "*:UpdateUser")
	beego.Router("/rbac/user/DelUser", &rbac.UserController{}, "*:DelUser")
	beego.Router("/rbac/user/index", &rbac.UserController{}, "*:Index")

	beego.Router("/rbac/node/AddAndEdit", &rbac.NodeController{}, "*:AddAndEdit")
	beego.Router("/rbac/node/DelNode", &rbac.NodeController{}, "*:DelNode")
	beego.Router("/rbac/node/index", &rbac.NodeController{}, "*:Index")
	beego.Router("/rbac/node/Getlist", &rbac.NodeController{}, "*:Getlist") //add by kang@20181120
	beego.Router("/rbac/node/GetPid", &rbac.NodeController{}, "*:GetPid")   //add by kang@20181120

	beego.Router("/rbac/group/AddGroup", &rbac.GroupController{}, "*:AddGroup")
	beego.Router("/rbac/group/UpdateGroup", &rbac.GroupController{}, "*:UpdateGroup")
	beego.Router("/rbac/group/DelGroup", &rbac.GroupController{}, "*:DelGroup")
	beego.Router("/rbac/group/index", &rbac.GroupController{}, "*:Index")

	beego.Router("/rbac/role/AddAndEdit", &rbac.RoleController{}, "*:AddAndEdit")
	beego.Router("/rbac/role/DelRole", &rbac.RoleController{}, "*:DelRole")
	beego.Router("/rbac/role/AccessToNode", &rbac.RoleController{}, "*:AccessToNode") //未用到
	beego.Router("/rbac/role/AddAccess", &rbac.RoleController{}, "*:AddAccess")
	beego.Router("/rbac/role/RoleToUserList", &rbac.RoleController{}, "*:RoleToUserList")
	beego.Router("/rbac/role/RoleToNodeList", &rbac.RoleController{}, "*:RoleToNodeList") //add by kang@20181121
	beego.Router("/rbac/role/AddRoleToUser", &rbac.RoleController{}, "*:AddRoleToUser")
	beego.Router("/rbac/role/DelRoleToUser", &rbac.RoleController{}, "*:DelRoleToUser") //add by kang@20181116
	beego.Router("/rbac/role/DelRoleToNode", &rbac.RoleController{}, "*:DelRoleToNode") //add by kang@20181116
	beego.Router("/rbac/role/Getlist", &rbac.RoleController{}, "*:Getlist")
	beego.Router("/rbac/role/GetridByuid", &rbac.RoleController{}, "*:GetRoleidByUserId") //add by kang@20181126
	beego.Router("/rbac/role/index", &rbac.RoleController{}, "*:Index")
}
