package rbac

import (
	//"encoding/json"
	//"fmt"
	"strconv"
	"strings"

	"github.com/astaxie/beego/orm"
	m "github.com/beego/admin/src/models"
)

type RoleController struct {
	CommonController
}

func (this *RoleController) Index() {
	if this.IsAjax() {
		page, _ := this.GetInt64("page")
		page_size, _ := this.GetInt64("rows")
		sort := this.GetString("sort")
		order := this.GetString("order")
		if len(order) > 0 {
			if order == "desc" {
				sort = "-" + sort
			}
		} else {
			sort = "Id"
		}
		roles, count := m.GetRolelist(page, page_size, sort)
		if len(roles) < 1 {
			roles = []orm.Params{}
		}
		this.Data["json"] = &map[string]interface{}{"code": 0, "msg": "", "count": count, "data": &roles}
		this.ServeJSON()
		return
	} else {
		//this.TplName = this.GetTemplatetype() + "/rbac/role.tpl"
		this.TplName = this.GetTemplatetype() + "/rolelist.html"
	}

}
func (this *RoleController) AddAndEdit() {
	r := m.Role{}
	if err := this.ParseForm(&r); err != nil {
		//handle error
		this.Rsp(false, err.Error())
		return
	}
	var id int64
	var err error
	Rid, _ := this.GetInt64("Id")
	if Rid > 0 {
		id, err = m.UpdateRole(&r)
	} else {
		id, err = m.AddRole(&r)
	}
	if err == nil && id > 0 {
		this.Rsp(true, "Success")
		return
	} else {
		this.Rsp(false, err.Error())
		return
	}

}

func (this *RoleController) DelRole() {
	Id, _ := this.GetInt64("Id")
	err := m.DelUserRole(Id) //增加删除user_role中的角色
	if err != nil {
		this.Rsp(false, err.Error())
	}
	err2 := m.DelNodeRole(Id) //增加删除node_role中的角色
	if err2 != nil {
		this.Rsp(false, err.Error())
	}
	status, err := m.DelRoleById(Id)
	if err == nil && status > 0 {
		this.Rsp(true, "Success")
		return
	} else {
		this.Rsp(false, err.Error())
		return
	}
}

func (this *RoleController) Getlist() {
	roles, _ := m.GetRolelist(1, 1000, "Id")
	if len(roles) < 1 {
		roles = []orm.Params{}
	}
	this.Data["json"] = &roles
	this.ServeJSON()
	return
}

func (this *RoleController) AccessToNode() {
	roleid, _ := this.GetInt64("Id")
	if this.IsAjax() {
		groupid, _ := this.GetInt64("group_id")
		nodes, count := m.GetNodelistByGroupid(groupid)
		list, _ := m.GetNodelistByRoleId(roleid)
		for i := 0; i < len(nodes); i++ {
			if nodes[i]["Pid"] != 0 {
				nodes[i]["_parentId"] = nodes[i]["Pid"]
			} else {
				nodes[i]["state"] = "closed"
			}
			for x := 0; x < len(list); x++ {
				if nodes[i]["Id"] == list[x]["Id"] {
					nodes[i]["checked"] = 1
				}
			}
		}
		if len(nodes) < 1 {
			nodes = []orm.Params{}
		}
		//this.Data["json"] = &map[string]interface{}{"total": count, "rows": &nodes}
		this.Data["json"] = &map[string]interface{}{"code": 0, "msg": "", "count": count, "data": &nodes}
		this.ServeJSON()
		return
	} else {
		//grouplist := m.GroupList()
		//b, _ := json.Marshal(grouplist)
		//this.Data["grouplist"] = string(b)
		//this.Data["roleid"] = roleid
		//this.TplName = this.GetTemplatetype() + "/rbac/accesstonode.tpl"
		this.TplName = this.GetTemplatetype() + "/accesstonode.html"
	}

}

func (this *RoleController) AddAccess() {
	roleid, _ := this.GetInt64("roleid")
	//group_id, _ := this.GetInt64("group_id")
	//err := m.DelGroupNode(roleid, group_id)
	//if err != nil {
	//	this.Rsp(false, err.Error())
	//}

	//err := m.DelNodeRole(roleid) //用户在修改授权时需要删除旧的授权
	//if err != nil {
	//	this.Rsp(false, err.Error())
	//}
	ids := this.GetString("ids")
	nodeids := strings.Split(ids, ",")
	for _, v := range nodeids {
		id, _ := strconv.Atoi(v)
		_, err := m.AddRoleNode(roleid, int64(id))
		if err != nil {
			this.Rsp(false, err.Error())
		}
	}
	this.Rsp(true, "success")

}

func (this *RoleController) RoleToUserList() {
	roleid, _ := this.GetInt64("Id")
	if this.IsAjax() {
		//users, count := m.Getuserlist(1, 1000, "Id")
		users, count := m.GetUserByRoleId(roleid)
		/*
			for i := 0; i < len(users); i++ {
				for x := 0; x < len(list); x++ {
					if users[i]["Id"] == list[x]["Id"] {
						users[i]["checked"] = 1
					}
				}
			}
			if len(users) < 1 {
				users = []orm.Params{}
			}
		*/
		if len(users) < 1 {
			users = []orm.Params{}
		}
		this.Data["json"] = &map[string]interface{}{"code": 0, "msg": "", "count": count, "data": &users}
		this.ServeJSON()
		return
	} else {
		this.Data["roleid"] = roleid
		//this.TplName = this.GetTemplatetype() + "/rbac/roletouserlist.tpl"
		this.TplName = this.GetTemplatetype() + "/roletouserlist.html"
	}
}

//add by kang@20181120
func (this *RoleController) RoleToNodeList() {
	roleid, _ := this.GetInt64("Id")
	if this.IsAjax() {
		nodes, count := m.GetNodeByRoleId(roleid)
		if len(nodes) < 1 {
			nodes = []orm.Params{}
		}
		this.Data["json"] = &map[string]interface{}{"code": 0, "msg": "", "count": count, "data": &nodes}
		this.ServeJSON()
		return
	} else {
		this.Data["roleid"] = roleid
		this.TplName = this.GetTemplatetype() + "/roletonodelist.html"
	}
}

func (this *RoleController) AddRoleToUser() {
	roleid, _ := this.GetInt64("Id")
	ids := this.GetString("ids")
	userids := strings.Split(ids, ",")
	//err := m.DelUserRole(roleid)
	//if err != nil {
	//	this.Rsp(false, err.Error())
	//}
	if len(ids) > 0 {
		for _, v := range userids {
			id, _ := strconv.Atoi(v)
			_, err := m.AddRoleUser(roleid, int64(id))
			if err != nil {
				this.Rsp(false, err.Error())
			}
		}
	}
	this.Rsp(true, "success")
}

//add by kang@20181116
func (this *RoleController) DelRoleToUser() {
	roleid, _ := this.GetInt64("Id")
	ids := this.GetString("ids")
	userids := strings.Split(ids, ",")
	if len(ids) > 0 {
		for _, v := range userids {
			id, _ := strconv.Atoi(v)
			err := m.DelUserRoleByUserId(roleid, int64(id))
			if err != nil {
				this.Rsp(false, err.Error())
			}
		}
	}
	this.Rsp(true, "success")
}

//add by kang@20181116
func (this *RoleController) DelRoleToNode() {
	roleid, _ := this.GetInt64("Id")
	ids := this.GetString("ids")
	nodeids := strings.Split(ids, ",")
	if len(ids) > 0 {
		for _, v := range nodeids {
			id, _ := strconv.Atoi(v)
			err := m.DelNodeRoleByNodeId(roleid, int64(id))
			if err != nil {
				this.Rsp(false, err.Error())
			}
		}
	}
	this.Rsp(true, "success")
}

//编辑用户信息时根据userid获取角色id by kang@20181126
func (this *RoleController) GetRoleidByUserId() {
	userid, _ := this.GetInt64("Id")
	roleids := m.GetRoleByUserId(userid)
	if len(roleids) < 1 {
		roleids = []orm.Params{}
	}
	this.Data["json"] = &roleids
	this.ServeJSON()
	return
}
