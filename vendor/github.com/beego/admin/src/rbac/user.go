package rbac

import (
	//"fmt"
	"strconv"
	"strings"

	m "github.com/beego/admin/src/models"
)

type UserController struct {
	CommonController
}

func (this *UserController) Index() {
	page, _ := this.GetInt64("page")
	page_size, _ := this.GetInt64("rows")
	sort := this.GetString("sort")
	order := this.GetString("order")
	if len(order) > 0 {
		if order == "desc" {
			sort = "-" + sort
		}
	} else {
		sort = "Id"
	}
	users, count := m.Getuserlist(page, page_size, sort)
	if this.IsAjax() {
		this.Data["json"] = &map[string]interface{}{"code": 0, "msg": "", "count": count, "data": &users}
		this.ServeJSON()
		return
	} else {
		//tree := this.GetTree()
		//this.Data["tree"] = &tree
		this.Data["users"] = &users
		//if this.GetTemplatetype() != "easyui" {
		//	this.Layout = this.GetTemplatetype() + "/public/layout.tpl"
		//}
		//this.TplName = this.GetTemplatetype() + "/rbac/user.tpl"
		this.TplName = this.GetTemplatetype() + "/adminlist.html"
	}

}

func (this *UserController) AddUser() {
	u := m.User{}
	Roleids := this.GetString("Roleid")
	roleidarr := strings.Split(Roleids, ",")
	if err := this.ParseForm(&u); err != nil {
		//handle error
		this.Rsp(false, err.Error())
		return
	}

	id, err := m.AddUser(&u)
	if err == nil && id > 0 {
		if len(roleidarr) > 0 {
			for _, v := range roleidarr {
				rid, _ := strconv.Atoi(v)
				_, err := m.AddRoleUser(int64(rid), id)
				if err != nil {
					this.Rsp(false, err.Error())
					return
				}
			}
		}
		this.Rsp(true, "Success")
		return
	} else {
		this.Rsp(false, err.Error())
		return
	}

}

func (this *UserController) UpdateUser() {
	if this.Ctx.Request.Method == "POST" {
		u := m.User{}
		if err := this.ParseForm(&u); err != nil {
			//handle error
			this.Rsp(false, err.Error())
			return
		}

		//更新角色
		Id, _ := this.GetInt64("Id")
		Roleids := this.GetString("Roleid")
		roleidarr := strings.Split(Roleids, ",")

		//删除user_roles中的用户角色 当用户更改不授权角色时需要删除对应的权限
		err1 := m.DelUserRoleByUserId(0, Id)
		if err1 != nil {
			this.Rsp(false, err1.Error())
		}

		if len(roleidarr) > 0 {
			for _, v := range roleidarr {
				rid, _ := strconv.Atoi(v)
				if rid > 0 {
					_, err := m.AddRoleUser(int64(rid), Id)
					if err != nil {
						this.Rsp(false, err.Error())
						return
					}
				}
			}
		}

		//更新用户信息
		//id, err := m.UpdateUser(&u)
		_, err := m.UpdateUser(&u)
		//if err == nil && id > 0 {
		if err == nil {
			this.Rsp(true, "Success")
			return
		} else {
			this.Rsp(false, "UpdateFail")
			return
		}
	} else {
		if this.IsAjax() {
			Id, _ := this.GetInt64("Id")
			user := m.GetUserById(Id)
			//fmt.Println(user)
			//this.Data["userid"] = Id
			//roleids := m.GetRoleByUserId(Id)
			//fmt.Println(roleids)
			this.Data["json"] = &map[string]interface{}{"data": &user}
			this.ServeJSON()
		} else {
			this.TplName = this.GetTemplatetype() + "/adminedit.html"
		}
	}
}

func (this *UserController) DelUser() {
	Id, _ := this.GetInt64("Id")

	status, err := m.DelUserById(Id)
	if err == nil && status > 0 {
		//删除user_roles中的用户角色
		err1 := m.DelUserRoleByUserId(0, Id)
		if err1 != nil {
			this.Rsp(false, err.Error())
		}
		this.Rsp(true, "Success")
		return
	} else {
		this.Rsp(false, err.Error())
		return
	}
}
