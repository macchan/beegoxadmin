# beegoxadmin

#### 介绍
利用beegoadmin 结合xadmin 模板来修改的后台界面。beegoadmin 源代码里的后台界面实在是太过丑陋。

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2018/1225/185632_022f8931_979382.png "login.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1225/185653_57f86c0e_979382.png "main.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1225/185719_448e263a_979382.png "role.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1225/185732_95e6969b_979382.png "node.png")

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)