package routers

import (
	"controllers"

	"github.com/astaxie/beego"
	"github.com/beego/admin"
)

func init() {
	admin.Run()
	//用户管理
	beego.Router("/xuser/List", &controllers.XuserController{}, "*:List")
	beego.Router("/xuser/AddUser", &controllers.XuserController{}, "*:AddUser")
	beego.Router("/xuser/UpdateUser", &controllers.XuserController{}, "*:UpdateUser")
	beego.Router("/xuser/DelUser", &controllers.XuserController{}, "*:DelUser")
	//产品管理
	beego.Router("/", &controllers.MainController{})
}
