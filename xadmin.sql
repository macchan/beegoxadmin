/*
Navicat MySQL Data Transfer

Source Server         : 本地mysql
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : xadmin

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2018-12-24 18:05:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES ('1', 'Privilege', '权限管理', '2', '1');
INSERT INTO `group` VALUES ('2', 'users', '用户管理', '2', '2');
INSERT INTO `group` VALUES ('3', 'network', '网络管理', '2', '3');
INSERT INTO `group` VALUES ('4', 'pods', '容器管理', '2', '4');
INSERT INTO `group` VALUES ('5', 'podsnode', '节点管理', '2', '5');
INSERT INTO `group` VALUES ('6', 'providerQa', '厂商质量监控', '2', '6');

-- ----------------------------
-- Table structure for node
-- ----------------------------
DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT '1',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `icons` varchar(200) DEFAULT NULL,
  `sorts` int(11) DEFAULT '1',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of node
-- ----------------------------
INSERT INTO `node` VALUES ('1', '权限管理', 'rbac', '1', '0', 'layui-icon-auz', '4', '', '2', '1');
INSERT INTO `node` VALUES ('2', '目录结构', 'node/index', '2', '1', null, '4', '', '2', '1');
INSERT INTO `node` VALUES ('3', 'node list', 'index', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('4', 'add or edit', 'AddAndEdit', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('5', 'del node', 'DelNode', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('6', '管理员', 'user/index', '2', '1', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('7', '管理员列表', 'Index', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('8', '添加管理员', 'AddUser', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('9', '更新管理员', 'UpdateUser', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('10', '删除管理员', 'DelUser', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('11', '目录分组', 'group/index', '2', '1', null, '3', '', '2', '1');
INSERT INTO `node` VALUES ('12', '分组列表', 'index', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('13', '添加分组', 'AddGroup', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('14', '修改分组', 'UpdateGroup', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('15', '删除分组', 'DelGroup', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('16', '角色管理', 'role/index', '2', '1', null, '2', '', '2', '1');
INSERT INTO `node` VALUES ('17', '角色列表', 'index', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('18', '添加编辑角色', 'AddAndEdit', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('19', '删除角色', 'DelRole', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('20', 'get roles', 'Getlist', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('21', 'show access', 'AccessToNode', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('22', 'add accsee', 'AddAccess', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('23', 'show role to userlist', 'RoleToUserList', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('24', 'add role to user', 'AddRoleToUser', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('27', 'Getlist', 'Getlist', '3', '2', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('28', 'GetPid', 'GetPid', '3', '2', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('29', 'RoleToNodeList', 'RoleToNodeList', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('30', 'DelRoleToUser', 'DelRoleToUser', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('31', 'DelRoleToNode', 'DelRoleToNode', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('32', 'GetridByuid', 'GetridByuid', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('33', '数据中心', 'datacenter', '1', '0', 'layui-icon-component', '2', '', '2', '2');
INSERT INTO `node` VALUES ('34', 'IP统计', 'ipcount', '2', '33', null, '1', '', '2', '2');
INSERT INTO `node` VALUES ('35', '网络管理', 'network', '1', '0', 'layui-icon-find-fill', '3', '', '2', '3');
INSERT INTO `node` VALUES ('36', 'ping检测', 'ping', '2', '35', null, '1', '', '2', '3');
INSERT INTO `node` VALUES ('37', '用户管理', 'xuser', '1', '0', 'layui-icon-user', '1', '', '2', '5');
INSERT INTO `node` VALUES ('38', '用户列表', 'List', '2', '37', '', '1', '', '2', '1');
INSERT INTO `node` VALUES ('39', '用户添加', 'AddUser', '3', '38', '', '1', '', '2', '2');
INSERT INTO `node` VALUES ('40', '用户更新', 'UpdateUser', '3', '38', '', '1', '', '2', '2');
INSERT INTO `node` VALUES ('41', '删除用户', 'DelUser', '3', '38', '', '1', '', '2', '2');

-- ----------------------------
-- Table structure for node_roles
-- ----------------------------
DROP TABLE IF EXISTS `node_roles`;
CREATE TABLE `node_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of node_roles
-- ----------------------------
INSERT INTO `node_roles` VALUES ('1', '1', '1');
INSERT INTO `node_roles` VALUES ('101', '1', '6');
INSERT INTO `node_roles` VALUES ('102', '2', '6');
INSERT INTO `node_roles` VALUES ('103', '3', '6');
INSERT INTO `node_roles` VALUES ('104', '4', '6');
INSERT INTO `node_roles` VALUES ('105', '5', '6');
INSERT INTO `node_roles` VALUES ('106', '27', '6');
INSERT INTO `node_roles` VALUES ('107', '28', '6');
INSERT INTO `node_roles` VALUES ('108', '6', '6');
INSERT INTO `node_roles` VALUES ('109', '7', '6');
INSERT INTO `node_roles` VALUES ('110', '8', '6');
INSERT INTO `node_roles` VALUES ('111', '9', '6');
INSERT INTO `node_roles` VALUES ('112', '10', '6');
INSERT INTO `node_roles` VALUES ('113', '11', '6');
INSERT INTO `node_roles` VALUES ('114', '12', '6');
INSERT INTO `node_roles` VALUES ('115', '13', '6');
INSERT INTO `node_roles` VALUES ('116', '14', '6');
INSERT INTO `node_roles` VALUES ('117', '15', '6');
INSERT INTO `node_roles` VALUES ('118', '16', '6');
INSERT INTO `node_roles` VALUES ('119', '17', '6');
INSERT INTO `node_roles` VALUES ('120', '18', '6');
INSERT INTO `node_roles` VALUES ('121', '19', '6');
INSERT INTO `node_roles` VALUES ('122', '20', '6');
INSERT INTO `node_roles` VALUES ('123', '21', '6');
INSERT INTO `node_roles` VALUES ('124', '22', '6');
INSERT INTO `node_roles` VALUES ('125', '23', '6');
INSERT INTO `node_roles` VALUES ('126', '24', '6');
INSERT INTO `node_roles` VALUES ('127', '29', '6');
INSERT INTO `node_roles` VALUES ('128', '30', '6');
INSERT INTO `node_roles` VALUES ('129', '31', '6');
INSERT INTO `node_roles` VALUES ('130', '32', '6');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin role', '超级管理员', 'I\'m a admin role', '2');
INSERT INTO `role` VALUES ('4', '', '技术支持', '', '2');
INSERT INTO `role` VALUES ('5', '', '监控', '', '2');
INSERT INTO `role` VALUES ('6', '', '普通管理员', '', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `nickname` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(32) NOT NULL DEFAULT '',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `lastlogintime` datetime DEFAULT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `nickname` (`nickname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '超级管理员', 'osgochina@gmail.com', 'I\'m admin', '2', null, '2015-07-24 10:06:49');
INSERT INTO `user` VALUES ('3', 'wangbikang', '26a939446c8db4dc1e8a75ec4f16ba86', 'wangbikang', 'wangbikang@136.com', 'myinfo test', '2', null, '2018-11-28 08:08:04');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('2', '7', '4');
INSERT INTO `user_roles` VALUES ('3', '7', '5');
INSERT INTO `user_roles` VALUES ('52', '3', '5');

-- ----------------------------
-- Table structure for xuser
-- ----------------------------
DROP TABLE IF EXISTS `xuser`;
CREATE TABLE `xuser` (
  `t_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `t_username` varchar(32) NOT NULL DEFAULT '',
  `t_password` varchar(32) NOT NULL DEFAULT '',
  `t_telephone` varchar(11) NOT NULL DEFAULT '',
  `t_email` varchar(32) NOT NULL DEFAULT '',
  `t_remark` varchar(200) DEFAULT NULL,
  `t_status` int(11) NOT NULL DEFAULT '2',
  `t_lastlogintime` datetime DEFAULT NULL,
  `t_createtime` datetime NOT NULL,
  PRIMARY KEY (`t_id`),
  UNIQUE KEY `username` (`t_username`) USING BTREE,
  UNIQUE KEY `telephone` (`t_telephone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xuser
-- ----------------------------
INSERT INTO `xuser` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '18620404948', 'osgochina@gmail.com', 'I\'m admin', '2', null, '2015-07-24 10:06:49');
INSERT INTO `xuser` VALUES ('3', 'wangbikang', '26a939446c8db4dc1e8a75ec4f16ba86', '12345678912', 'wangbikang@136.com', 'myinfo test', '2', null, '2018-11-28 08:08:04');
INSERT INTO `xuser` VALUES ('5', 'testone', 'dea404003c3a80819f73187842f5d1de', '15623523512', 'testone@qq.com', 'test', '2', null, '2018-12-12 09:36:16');
